const limitFunctionCallCount = (cb,n) =>{
      let counter = 0;
      return function limit() {
           if (counter < n) {
               counter += 1
               return cb(counter)
           }
           return null;
      };
   
      
   };
   
   module.exports = limitFunctionCallCount;   