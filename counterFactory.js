function counterFactory(){
      let counter = 0;
     function increment (){

           return counter += 1
      }
     function decrement (){

            return counter -= 1
      }
       
      return {
             increment,
             decrement
          }
}

module.exports = counterFactory